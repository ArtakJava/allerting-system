# Description

My [swagger documentation](../main/openapi.yaml) for Allerting System. In this documentation you can see operations description, status codes for methods and so on.

## Requirements
### 1. Functional

The system must:  
· can create new users;
· the user can save his tasks with due dates;  
· the user must have access to read his tasks;  
· the user must have access to update his tasks;  
· the user should be able to change his tasks.

### 2. Non-Functional

The system should be:  
· available 24/7;  
· secure;  
· scalable;  

## Model description

### Entities

#### User:  
id: for identify user  
userName: User login  
firstName: User firstname  
lastName: User lastname  
email: User email  
phone: User phone number

#### Task:  
id: for identify user  
title: User login  
status: Current status for task  
createdOn: Date when task created  
onDate: When need to do task  
description: Task description  